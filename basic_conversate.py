def greeting(name):
    print("Hello {}. I see you're looking at models. How can I help?".format(name))
    
    
def small_tak_conversate(input_string):
    input_string_lower = input_string.lower()
    if 'yes' in input_string_lower:
        print('Great! What is your issue with:')
        print('Model Management')
        print('Administration')
        print('Line Items')
        print('Lists')
        print('Modules')
        print('Other')
    elif 'yea' in input_string_lower:
        print('Great! What is your issue with:')
        print('Model Management')
        print('Administration')
        print('Line Items')
        print('Lists')
        print('Modules')
        print('Other')
    elif 'yeah' in input_string_lower:
        print('Great! What is your issue with:')
        print('Model Management')
        print('Administration')
        print('Line Items')
        print('Lists')
        print('Modules')
        print('Other')
    else:
        print('Okay, no problem! Tell us your issue.')
        
        
def take_category(input_string):
    input_string_lower = input_string.lower()
    if input_string_lower == 'model management':
        print('Thank you! What issue are you facing with Model Management?')
    elif input_string_lower == 'administration':
        print('Thank you! What issue are you facing with Workspace Administration?')
    elif input_string_lower == 'line items':
        print('Thank you! What issue are you facing with your Line Items?')
    elif input_string_lower == 'lists':
        print('Thank you! What issue are you facing with your Lists?')
    elif input_string_lower == 'modules':
        print('Thank you! What issue are you facing with your Modules?')
        

def take_input():
    print('\n')
    string_input = input()
    print('\n')
    return string_input
    