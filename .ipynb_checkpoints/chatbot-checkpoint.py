from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer,ChatterBotCorpusTrainer
from chatterbot.corpus import load_corpus
import pandas as pd
import sys,os

class anaplan_chatterbot():
    def __init__(self):
        pass
    
    def _initialize_bot(self):
        self.bot = ChatBot('Anabot',read_only=True,logic_adapters=['chatterbot.logic.BestMatch'])
    
    def _get_ymls(self):
        self.file_names = []
        for file in os.listdir("./"):
            if file.endswith(".yml"):
                self.file_names.append(file)
                
    def _train_on_ymls(self):    
        trainer = ChatterBotCorpusTrainer(self.bot)
        for each_file in self.file_names:
            trainer.train("./{}".format(each_file))
            
    def _ask_bot(self,question):
        response = self.bot.get_response(question)
        return response