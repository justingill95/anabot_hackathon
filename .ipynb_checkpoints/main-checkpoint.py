from chatbot import anaplan_chatterbot
from basic_conversate import *
import yaml
import os
import warnings
import spacy
import pandas as pd
import re
warnings.filterwarnings("ignore")

def pretty_print(s):
    pattern = re.compile("(\d. )")
    for i,each in enumerate(re.split("(\d. )", s)):
        if(pattern.match(each)):
            print('\n')
        print(each)

if __name__ == "__main__":
    print('\n')
    print('Bot:')
    greeting('Justin')
    
    string_input = take_input()
    
    #small_tak_conversate(string_input)
    
    #string_input = take_input()
    
    #take_category(string_input)
    
    #string_input = take_input()
    
    chatbot = anaplan_chatterbot()
    chatbot._initialize_bot()
    chatbot._get_ymls()
    chatbot._train_on_ymls()
    
    file_names = []
    for file in os.listdir("./"):
        if file.endswith(".yml"):
            file_names.append(file)
            
    lists = []
    for each in file_names:
        with open('./{}'.format(each)) as f:
            # use safe_load instead load
            dataMap = yaml.safe_load(f)
        lists.append(dataMap['conversations'])
        
    questions =[]
    for each in lists:
        for each2 in each:
            questions.append(each2[0])

    nlp = spacy.load("en_core_web_sm")
    doc1 = nlp(string_input)
    question_df = pd.DataFrame()
    for token in doc1:
        question_df = question_df.append([[token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                token.shape_, token.is_alpha, token.is_stop]])

    question_df.columns = ['text','lemma','pos','tag','dep','shape','is_alpha','is_stop']
    question_list = list(question_df['lemma'])
    filtered = list(filter(lambda val: val !=  '-PRON-', question_list))

    lemma_question = ' '.join(filtered)

    doc1 = nlp(lemma_question)

    doc_df = pd.DataFrame()
    for each in questions:
        doc2 = nlp(each)
        doc_df = doc_df.append([[doc1,doc2,doc1.similarity(doc2)]])
        
    sorteds = doc_df.sort_values(2,ascending=False)
    
    string = str(sorteds.iloc[0][1])
    counts =[]
    for i,each_row in sorteds.iterrows():

        list1 = set(list(each_row[0]))
        list2 = set(list(each_row[1]))

        list1 = set([str(x) for x in list1])
        list2 = set([str(x) for x in list2])
        how_many  = len(list1.intersection(list2))
        counts.append(how_many)

    sorteds.columns =['question','keyword_match','similarity']
    sorteds['counts_matched'] = counts
    sorteds = sorteds.sort_values(['counts_matched','similarity'],ascending=False)
    string = str(sorteds.iloc[0]['keyword_match'])
    resp=chatbot._ask_bot(string)
    
    print('Bot:')
    pretty_print(str(resp))
    
    string_input = take_input()
    
    doc1 = nlp(string_input)
    question_df = pd.DataFrame()
    for token in doc1:
        question_df = question_df.append([[token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                token.shape_, token.is_alpha, token.is_stop]])

    question_df.columns = ['text','lemma','pos','tag','dep','shape','is_alpha','is_stop']
    question_list = list(question_df['lemma'])
    filtered = list(filter(lambda val: val !=  '-PRON-', question_list))

    lemma_question = ' '.join(filtered)

    doc1 = nlp(lemma_question)

    doc_df = pd.DataFrame()
    for each in questions:
        doc2 = nlp(each)
        doc_df = doc_df.append([[doc1,doc2,doc1.similarity(doc2)]])
        
    sorteds = doc_df.sort_values(2,ascending=False)
    
    string = str(sorteds.iloc[0][1])
    counts =[]
    for i,each_row in sorteds.iterrows():

        list1 = set(list(each_row[0]))
        list2 = set(list(each_row[1]))

        list1 = set([str(x) for x in list1])
        list2 = set([str(x) for x in list2])
        how_many  = len(list1.intersection(list2))
        counts.append(how_many)

    sorteds.columns =['question','keyword_match','similarity']
    sorteds['counts_matched'] = counts
    sorteds = sorteds.sort_values(['counts_matched','similarity'],ascending=False)
    string = str(sorteds.iloc[0]['keyword_match'])
    resp=chatbot._ask_bot(string)
    
    print('Bot:')
    pretty_print(str(resp))
    string_input = take_input()
    
    s = 'Here are some areas to check in your lists. 1. Time set as a key 2. Code is combination of properties 3. Does the list use a code? 4. Show more...'
    print('Bot:')
    pretty_print(s)
    
    string_input = take_input()
    
    s = 'Why is this a problem? \nThinking about this from a list creation point of view, do you really want to create a new list item for every date, or do you want to create the list item once? If you include Time as part of the key, your list will be unnecessarily large. \nHow to correct: \nDates are data and should not be part of a code. Remove the dates and use time references outside of the list. \n \nRead more [Link]'
    print('Bot:')
    print(s)
    
    s = 'Was this helpful?\n 1. Yes 2. No'
    
    pretty_print(s)
    
    string_input = take_input()
    input_string_lower = string_input.lower()
    test_list = ['yes','yea','yeah','sure','please']
    res = any(ele in input_string_lower for ele in test_list)
    if(res):
        print('Bot:')
        pretty_print('Back to help list\n')
        
    s = 'There are a few steps in this checklist to help your model run more smoothly. 1. Lists 2. Modules 3. Line Items 4. Show more..'
    pretty_print(s)
    string_input = take_input()
    print('Bot:')
    s = 'Goodbye Justin, I have ended our session.\n '
    pretty_print(s)
    
    '''
    res=True
    while(res==True):
        string_input = take_input()
        input_string_lower = string_input.lower()
        test_list = ['yes','yea','yeah','sure','please']
        res = any(ele in input_string_lower for ele in test_list)
        if(res):
            print('Thank you is there anything else I can help you with!')
        else:
            print("Thank you for using Anabot. Have a good day!")
            res = False
            break

        string_input = take_input()

        input_string_lower = string_input.lower()
        test_list = ['yes','yea','yeah','sure','please']
        res = any(ele in input_string_lower for ele in test_list)

        if(res):
            print('What may we help you with?')
            string_input = take_input()
            doc1 = nlp(string_input)
            question_df = pd.DataFrame()
            for token in doc1:
                question_df = question_df.append([[token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                        token.shape_, token.is_alpha, token.is_stop]])

            question_df.columns = ['text','lemma','pos','tag','dep','shape','is_alpha','is_stop']
            question_list = list(question_df['lemma'])
            filtered = list(filter(lambda val: val !=  '-PRON-', question_list))

            lemma_question = ' '.join(filtered)

            doc1 = nlp(lemma_question)

            doc_df = pd.DataFrame()
            for each in questions:
                doc2 = nlp(each)
                doc_df = doc_df.append([[doc1,doc2,doc1.similarity(doc2)]])

            sorteds = doc_df.sort_values(2,ascending=False)

            string = str(sorteds.iloc[0][1])
            counts =[]
            for i,each_row in sorteds.iterrows():

                list1 = set(list(each_row[0]))
                list2 = set(list(each_row[1]))

                list1 = set([str(x) for x in list1])
                list2 = set([str(x) for x in list2])
                how_many  = len(list1.intersection(list2))
                counts.append(how_many)

            sorteds.columns =['question','keyword_match','similarity']
            sorteds['counts_matched'] = counts
            sorteds = sorteds.sort_values(['counts_matched','similarity'],ascending=False)
            string = str(sorteds.iloc[0]['keyword_match'])
            resp=chatbot._ask_bot(string)
            
            pretty_print(str(resp))
            print('\nDid this help you in resolving your issue?')
            string_input = take_input()
            input_string_lower = string_input.lower()
            test_list = ['yes','yea','yeah','sure','please']
            res2 = any(ele in input_string_lower for ele in test_list)
            if(res2==True):
                print("\nThank you for using Anabot. Have a good day!")
                res = False
            else:
                print("\nWe're sorry, we can direct your case to customer care, would you like that?")
                res = False
        else:
            print("\nThank you for using Anabot. Have a good day!")
            res = False
            break
    '''

